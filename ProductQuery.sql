CREATE TABLE PRODUCT (

	productId INT,
	productName varchar(25),
	productPrice BIGINT,
	productImage VARCHAR(150)

);

DROP TABLE PRODUCTS;

INSERT INTO PRODUCT(productId, productName,productPrice,productImage)
VALUES(1, 'Green Tea', 30, 'Images/GreenTea.jpg');

INSERT INTO PRODUCT(productId, productName,productPrice,productImage)
VALUES(2, 'Black Tea', 30, 'Images/BlackTea.jpg');

INSERT INTO PRODUCT(productId, productName,productPrice,productImage)
VALUES(3, 'Black Coffee', 30, 'Images/BlackCoffee.jpg');

INSERT INTO PRODUCT(productId, productName,productPrice,productImage)
VALUES(4, 'Coffee', 30, 'Images/Coffee.jpg');

INSERT INTO PRODUCT(productId, productName,productPrice,productImage)
VALUES(5, 'Bournvita', 30, 'Images/Bournvita.jpg');





