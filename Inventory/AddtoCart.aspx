﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddtoCart.aspx.cs" Inherits="_AddtoCart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 786px;
            width: 675px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <p>
            <asp:Label ID="lbl1" runat="server" Font-Bold="True" Font-Size="Large" Text="Satguru's Amrit-Tulya-Tea Shop"></asp:Label>
        </p>
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" style="text-align: center" Text="Cart"></asp:Label>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink1" runat="server" Font-Italic="True" NavigateUrl="Default.aspx">Continue Shoping</asp:HyperLink>
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Height="20px" Width="539px">
            <Columns>
                <asp:BoundField DataField="sno" HeaderText="S. No.">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="productId" HeaderText="Product Id">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="productName" HeaderText="Product Name">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="productPrice" HeaderText="Price">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:ImageField DataImageUrlField="productImage" HeaderText="Image">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:ImageField>
            </Columns>
            <HeaderStyle Height="20px" />
        </asp:GridView>
    </form>
</body>
</html>
